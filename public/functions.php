<?php
/**
 * Register routes and callbacks
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
function wp_get_menu_array($current_menu) {

    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();

    foreach($array_menu as $parent_menu) {
        if($parent_menu->menu_item_parent == "0") {
            $menu_holder = [];
            $menu_holder['ID']      =   $parent_menu->ID;
            $menu_holder['title']   =   $parent_menu->title;
            $menu_holder['url']     =   $parent_menu->url;
            $menu_holder['slug']     =  wp_make_link_relative($parent_menu->url);
            $menu_holder['children']=   array();
            $menu[] = $menu_holder;
        }
    }

    foreach($array_menu as $submenu_item) {
        if($submenu_item->menu_item_parrent != "0") {
            $submenu_holder = [];
            $submenu_holder['ID']      =   $submenu_item->ID;
            $submenu_holder['title']   =   $submenu_item->title;
            $submenu_holder['url']     =   $submenu_item->url;
            $submenu_holder['slug']     =  wp_make_link_relative($submenu_item->url);
            $indexOfParentMenu = null;
            foreach($menu as $idx => $parent_menu) {
                if(intval($parent_menu['ID']) == intval($submenu_item->menu_item_parent)) {
                    $indexOfParentMenu = $idx;
                }
            }
            if($indexOfParentMenu) {
                $menu[$indexOfParentMenu]['children'][] = $submenu_holder;
            }
        }
    }

    return $menu;

}


add_action("rest_api_init", function () {

    register_rest_route("mc", "/menu/", array(
        "methods" => "GET",
        "callback" => function () {
            $pages = wp_get_menu_array("mainNavigation");

            return $pages;
        }
    ));

    register_rest_route("mc", "/carousel/", array(
        "methods" => "GET",
        "callback" => function () {
            $postTags = get_terms('portfolio_tag');

            foreach($postTags as $postTag) {
                if($postTag->name == "featured") {
                    $featuredTagId = $postTag->term_id;
                    break;
                }
            }

            if(isset($featuredTagId)) {
                $tax_query = array(
                    array(
                        'taxonomy' => 'portfolio_tag',
                        'field' => 'id',
                        'terms' => $featuredTagId,
                        'include_children' => false,
                    ),
                );
            } else {
                $tax_query = null;
            }

            $args = array(
                'posts_per_page' => 5, // Fallback for no featured posts
                'post_type' => 'portfolio',
                'post_status' => 'publish',
            );

            if($tax_query) {
                $args['tax_query'] = $tax_query;
                $args['posts_per_page'] = -1;
            }

            $pages = get_posts($args);

            foreach($pages as $idx => $page) {
                $thumbnails = [
                    'small'  => get_the_post_thumbnail_url($page->ID, 'thumbnail'),
                    'medium' => get_the_post_thumbnail_url($page->ID, 'large'),
                    'large'  => get_the_post_thumbnail_url($page->ID),
                ];
                if($thumbnails) {
                    $pages[$idx] -> featured_image = $thumbnails;
                }

                $postTerms = get_the_terms($page->ID, 'portfolio_category');

                if(count($postTerms) > 0) {
                    $pages[$idx] -> category = $postTerms[0]->slug;
                }

                $postTags = get_the_terms($page->ID, 'portfolio_tag');
                if($postTags) {
                    $pages[$idx] -> tags = $postTags;
                } else {
                    $pages[$idx] -> tags = [];
                }

                $pages[$idx] -> altText = $page->post_name;

            }
            return $pages;
        }
    ));

    register_rest_route("mc", "/portfolio/(?P<category>[a-zA-Z0-9-]+)/", array(
        "methods" => "GET",
        "callback" => function ($request ) {
            $limit = 5;
            $page = (int)$request['page'] ?? '0';
            $portfolioCategoryName = $request['category'];

            $portfolioCategory = get_term_by('slug', $portfolioCategoryName, 'portfolio_category');

            if(!$portfolioCategory) {
                return returnError();
            }

            $args = array(
                'posts_per_page' => $limit,
                'offset' => $limit * $page,
                'tax_query' => array (
                    array(
                        'taxonomy' => 'portfolio_category',
                        'field' => 'slug',
                        'terms' => $portfolioCategory->slug,
                    ),
                ),
                'post_type' => 'portfolio',
                'post_status' => 'publish',
            );
            $portfolioItems = get_posts($args);

            if(count($portfolioItems) > 0) {
                foreach($portfolioItems as $idx => $portfolioItem) {
                    $portfolioFeaturedImage = get_the_post_thumbnail_url($portfolioItem->ID, 'medium');
                    if($portfolioFeaturedImage) {
                        $portfolioItems[$idx] -> featured_image = $portfolioFeaturedImage;
                    }

                }
            }

            $postsNumber = (int)$portfolioCategory->count;

            return [
                "categoryName" => $portfolioCategory->name,
                "categoryItems" => $portfolioItems,
                "currentPage" => $page,
                "hasMore" => $postsNumber > $limit && ($page * $limit) * 2 < $postsNumber,
            ];
        }
    ));

    register_rest_route("mc","/portfolio/detail/(?P<id>(.*)+)/", array(
        "methods" => "GET",
        "callback" => function($request) {
            $portfolioSlug = $request['id'];
            if(!$portfolioSlug) {
                return returnError();
            }
            $portfolioItem = get_page_by_path($portfolioSlug, OBJECT, 'portfolio');
            if(is_null($portfolioItem)) {
                return returnError();
            } else {
                $postContent = $portfolioItem->post_content;
                $portfolioDetails = [];

                // Parse shortcodes
                $shortcodesParsed = shortcode_parse_atts($postContent);

                if(count($shortcodesParsed) > 0) {
                    switch($shortcodesParsed[0]) {
                        case "[gallery":
                            $photoIds = explode(',', $shortcodesParsed[1]);
                            foreach($photoIds as $photoId) {
                                $photo = get_post($photoId);
                                if($photo) {
                                    $photo->images = [
                                        'small'  => wp_get_attachment_image_src($photo->ID, 'thumbnail')[0],
                                        'medium' => wp_get_attachment_image_src($photo->ID, 'medium')[0],
                                        'large'  => wp_get_attachment_image_src($photo->ID, 'medium_large')[0],
                                    ];
                                    $portfolioDetails[] = $photo;
                                }
                            }
                            break;
                        case "[video":
                            $pattern = '/(https:\/\/.*)"]/';
                            $postContent = $shortcodesParsed[1];
                            preg_match($pattern, $postContent, $re);

                            $portfolioDetails[] = array(
                                "ID" => 0,
                                "post_type" => "attachment_video",
                                "url" => $re[1],
                            );
                            break;
                        default:
                            //nothing;
                            break;
                    }
                }

                $portfolioItem->post_content = $portfolioDetails;
                return $portfolioItem;
            }
        }
    ));

    register_rest_route("mc", "/contactForm", array(
        "methods" => "POST",
        "callback" => function($request) {
            $to = "test@test.com";
            $params = $request->get_params();

            $name = $params['name'];
            $email = $params['email'];
            $message = "From:".$name." (".$email.")"."\n".$params['message'];

            $mailSent = wp_mail($to, "New contact request", $message);
            if($mailSent) {
                return ["message" => "success"];
            } else {
                return ["error" => "an error occured"];
            }
        }
    ));
});


function returnError() {
    return new WP_REST_Response(["error" => "Not Found"],404);
}